import {DetailMovie} from './helper';
export async function render(): Promise<void> {
    // TODO render your app here
    const api_key: string = 'd5e79b4056426ebafbf190f96082c873';
    const movie_id: number = 1;
    const storage: Storage = window.sessionStorage;
    var currentType: { name: string, url: string, page: number} = null;
    const urlSearch: string = `https://api.themoviedb.org/3/search/movie?api_key=${api_key}`;
    const urlTopRated: string = `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}`;
    const urlPpopular: string = `https://api.themoviedb.org/3/movie/popular?api_key=${api_key}`;
    const urlUpcoming: string = `https://api.themoviedb.org/3/movie/upcoming?api_key=${api_key}`;
    const urlLatest: string = `https://api.themoviedb.org/3/movie/latest?api_key=${api_key}`;
    const urlDetail: string = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}`;

    const onRandomMovie: HTMLElement = document.querySelector('#random-movie');
    const onPopular: HTMLElement = document.querySelector('#popular');
    const onUpcoming: HTMLElement = document.querySelector('#upcoming');
    const onTopRated: HTMLElement = document.querySelector('#top_rated');

    const onSearch: HTMLElement = document.querySelector('form #submit');
    const onFavoriteMovie: HTMLElement = document.querySelector('.navbar-toggler');
    const onLoadMore: HTMLElement = document.querySelector('#load-more');

    const getCards: HTMLElement = document.querySelectorAll('.card');
    const getFilmContainer: HTMLElement = document.querySelector('#film-container');
    // const getSVGIcon: HTMLElement = document.querySelector('svg');
    const getFavoriteMovies: HTMLElement = document.querySelector('#favorite-movies');

    const http = async (urlStr: string) => await fetch(urlStr)
        .then( response => response.json() );

    const getDetail = async  (urlDetail:string) => await http(urlDetail)
        .then((data) => {
            const res = data;
            return data;
        });

    const byDeafult = (()=>{
        getFilmContainer.innerHTML='';
        if(storage !== null){
            Object.keys(storage).forEach(key =>{
                const movie_id = storage.getItem(key);
                const urlString = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}`;
                const detailInfo = getDetail(urlString);
                detailInfo.then(
                    obj => {
                        const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );
                        let isFav ="#ff000078";
                        if(storage !== null) {
                            let inStorage = false;
                            const checkId = Object.keys(storage).forEach(key => {
                                let toNum = +storage.getItem(key);
                                if( toNum === obj.id) inStorage = true;
                            })
                            isFav = inStorage ? "#FF0019" : "#ff000078";
                        }
                        const addBlock = addContent(info,isFav);
                        const blockWrapper   = document.createElement('div');
                        blockWrapper.classList.add("col-12", "p-2");
                        blockWrapper.innerHTML= addBlock;
                        getFilmContainer.append(blockWrapper);

                    }
                );
            })
        }

    })();

    const getBySearch = async (urlSearch: string) => http(urlSearch)
        .then((data) => {
            getFilmContainer.innerHTML=""
            const res = data.results;
            let i =0;
            res.map(obj => {
                const info: DetailMovie =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );

                let isFav ="#ff000078";
                if(storage !== null) {
                    let inStorage = false;
                    const checkId = Object.keys(storage).forEach(key => {
                        inStorage = storage.getItem(key) === obj.id ? true : false;
                    })
                    let isFav = inStorage ? "#FF0019" : "#ff000078";
                }
                const addBlock = addContent(info, isFav);
                const blockWrapper   = document.createElement('div');
                blockWrapper.classList.add( "col-lg-3", "col-md-4", "col-12", "p-2");
                blockWrapper.innerHTML= addBlock;
                getFilmContainer.append(blockWrapper);


                i++;

            })

        });




    const getRandomMovie = (() =>{
        if(storage !== null && storage.length){
            const lengthStorage = storage.length;
            const randNum = Math.random() * lengthStorage;
            const randomKey = storage.key(randNum);
            const movie_id = storage.getItem(randomKey);
            const urlString = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}`;
            const detailInfo = getDetail(urlString);
            detailInfo.then(
                obj => {
                    const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );

                    onRandomMovie.querySelector('#random-movie-name').innerHTML= info._title;
                    onRandomMovie.querySelector('#random-movie-description').innerHTML= info._description;
                }
            );
        }
    })();


    const getByPopular = async (urlPopular:string) => await http(urlPopular)
        .then((data) => {

            const res = data.results;
            let i = 0;
            res.map(obj => {
                const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );
                let isFav ="#ff000078";
                if(storage !== null) {
                    let inStorage = false;
                    const checkId = Object.keys(storage).forEach(key => {
                        let toNum = +storage.getItem(key);
                        if( toNum === obj.id) inStorage = true;
                    })
                     isFav = inStorage ? "#FF0019" : "#ff000078";
                }
                const addBlock = addContent(info, isFav);
                const blockWrapper   = document.createElement('div');
                blockWrapper.classList.add( "col-lg-3", "col-md-4", "col-12", "p-2");
                blockWrapper.innerHTML= addBlock;
                getFilmContainer.append(blockWrapper);
                i++;

            })
        });

    const getByUpcoming = async( urlUpcoming:string )=> await http(urlUpcoming)
        .then((data) => {
            const res = data.results;
            let i = 0;

            res.map( obj => {
                const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );
                let isFav ="#ff000078";
                if(storage !== null) {
                    let inStorage = false;
                    const checkId = Object.keys(storage).forEach(key => {
                        let toNum = +storage.getItem(key);
                        if( toNum === obj.id) inStorage = true;
                    })
                    isFav = inStorage ? "#FF0019" : "#ff000078";
                }
                const addBlock = addContent(info,isFav);
                const blockWrapper   = document.createElement('div');
                blockWrapper.classList.add( "col-lg-3", "col-md-4", "col-12", "p-2");
                blockWrapper.innerHTML= addBlock;
                getFilmContainer.append(blockWrapper);
                i++;

            })
        });
    const getByTopRated = async ( urlTopRelated: string) => await http(urlTopRelated)
        .then((data) => {

            const res = data.results;
            let i = 0;
            res.map( obj => {
                const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );
                let isFav ="#ff000078";
                if(storage !== null) {
                    let inStorage = false;
                    const checkId = Object.keys(storage).forEach(key => {
                        let toNum = +storage.getItem(key);
                        if( toNum === obj.id) inStorage = true;
                    })
                    isFav = inStorage ? "#FF0019" : "#ff000078";
                }
                const addBlock = addContent(info, isFav);
                const blockWrapper   = document.createElement('div');
                blockWrapper.classList.add( "col-lg-3", "col-md-4", "col-12", "p-2");
                blockWrapper.innerHTML= addBlock;
                getFilmContainer.append(blockWrapper);
                 i++;

            })
        });

    const  handleFavoriteMovies = () => {
        getFavoriteMovies.innerHTML='';
        if(storage !== null){
            Object.keys(storage).forEach(key =>{
                const movie_id = storage.getItem(key);
                const urlString = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}`;
                const detailInfo = getDetail(urlString);
                detailInfo.then(
                obj => {
                    const info =  new DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date );
                    let isFav ="#ff000078";
                    if(storage !== null) {
                        let inStorage = false;
                        const checkId = Object.keys(storage).forEach(key => {
                            let toNum = +storage.getItem(key);
                            if( toNum === obj.id) inStorage = true;
                        })
                        isFav = inStorage ? "#FF0019" : "#ff000078";
                    }
                    const addBlock = addContent(info,isFav);
                    const blockWrapper   = document.createElement('div');
                    blockWrapper.classList.add("col-12", "p-2");
                    blockWrapper.innerHTML= addBlock;
                    getFavoriteMovies.append(blockWrapper);

                    }
                );
            })
        }

    };
    onSearch.addEventListener('click', (e) => {
        const searchMovies = document.querySelector('form #search').value;
        const searchQuery = `&query=${searchMovies}`;
        const urlString = `${urlSearch}${searchQuery}`;
        getBySearch(urlString);
        return true;
    });

    onPopular.addEventListener('click', () => {
        if(currentType!==null && currentType.name !=='urlPpopular') getFilmContainer.innerHTML='';
        setCurrentData( 'urlPopular' ,  urlPpopular,   1 );
            getByPopular(urlPpopular);
        return true;
    });
    onUpcoming.addEventListener('click', () => {
        if(currentType!==null && currentType.name !=='urlUpcoming') getFilmContainer.innerHTML='';

        setCurrentData( 'urlUpcoming' ,  urlUpcoming,   1 );
        getByUpcoming(urlUpcoming);
        return true;
    });
    onTopRated.addEventListener('click', () => {
        if(currentType!==null && currentType.name !=='urlTopRated') getFilmContainer.innerHTML='';

        setCurrentData( 'urlTopRated' ,  urlTopRated,   1 );
        getByTopRated(urlTopRated);
        return true;
    });

    onFavoriteMovie.addEventListener('click', () => {
        handleFavoriteMovies();
        return true;
    });

    onLoadMore.addEventListener('click', () => {
        const currentPage = currentType !== null ? currentType.page : 1;
        const currentPath = currentType!==null ? currentType.url : urlPpopular;
        const currentName = currentType!==null ? currentType.name : 'urlPpopular';
        const searchQuery = `&page=${currentPage}`;
        const urlString = `${currentPath}${searchQuery}`;

        setCurrentData( currentName ,  currentPath,  currentPage + 1 );

        switch (currentName) {
            case  'urlPopular':
                setCurrentData( 'urlPopular' ,  currentPath,  currentPage + 1 );
                getByPopular(urlString);
                return;
            case 'urlTopRated':
                setCurrentData( 'urlTopRated' ,  currentPath,   currentPage + 1 );
                getByTopRated(urlString);
                return;

            case 'urlUpcoming':
                setCurrentData( 'urlUpcoming' ,  currentPath,  currentPage + 1 );
                getByUpcoming(urlString);

                return;
        }
        return true;
    });

   const setCurrentData = (name: string, url: string ,num: number) => {
      return currentType = {name: name, url: url, page: num  };
   }


    getFilmContainer.addEventListener('click', (e) => {
        let  id;
        if(e.target.tagName==='path'){
            id = e.target.parentNode.parentNode.id

        }
        if(e.target.tagName ==='svg' || e.target.tagName ==='IMG') {
            id = e.target.parentNode.id
        }
        let inStorage = false;
        const checkId = Object.keys(storage).forEach(key => {
            console.log(storage.getItem(key));
            if (storage.getItem(key) === id)  inStorage = true;
        })
        if (inStorage) storage.removeItem(`id_${id}`)
        else  storage.setItem(`id_${id}`,id);
                onUpdatePage();
            })

    getFavoriteMovies.addEventListener('click', e => {
        let  id;
        if(e.target.tagName==='path'){
            id = e.target.parentNode.parentNode.id

        }
        if(e.target.tagName ==='svg' || e.target.tagName ==='IMG') {
            id = e.target.parentNode.id
        }
        let inStorage = false;
        const checkId = Object.keys(storage).forEach(key => {
            if (storage.getItem(key) === id) inStorage = true;
        })

        if (inStorage) {
            storage.removeItem(`id_${id}`)
            handleFavoriteMovies()
        }
        else  storage.setItem(`id_${id}`,id);
        onUpdatePage();

    });

    const addPreloader = () => {
        const blockWrapper   = document.createElement('div');
        blockWrapper.classList.add( "col-lg-3", "col-md-4", "col-12", "p-2");
        blockWrapper.innerHTML= '<i class="fa fa-spinner" aria-hidden="true"></i>';
        return getFilmContainer.prepend(blockWrapper);
    }

      const onUpdatePage = () => {
          getFilmContainer.innerHTML='';
        const currentPage = currentType !== null ? currentType.page : 1;
        const currentPath = currentType!==null ? currentType.url : urlPpopular;
        const currentName = currentType!==null ? currentType.name : 'urlPpopular';
        const searchQuery = `&page=${currentPage}`;
        const urlString = `${currentPath}${searchQuery}`;
        switch (currentName) {
            case  'urlPopular':
                getByPopular(urlString);
                return;
            case 'urlTopRated':
                getByTopRated(urlString);
                return;

            case 'urlUpcoming':
                getByUpcoming(urlString);

                return;
        }
        return true;
    };

   const addContent =  (obj: DetailMovie, isFavorite: string) => {
           const insBlock  = `
                    <div class="card shadow-sm" id="${obj._id}">
                        <img
                            src="https://image.tmdb.org/t/p/original/${obj._image}"
                        />
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            stroke="red"
                            fill= ${isFavorite}
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                fill-rule="evenodd"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
                        <div class="card-body">
                            <p class="card-text truncate">
                                 ${obj._description}
                            </p>
                            <div
                                class="
                                    d-flex
                                    justify-content-between
                                    align-items-center
                                "
                            >
                                <small class="text-muted">${obj._data}</small>
                            </div>
                        </div>
                    </div>`;
           return insBlock;
   };

}
