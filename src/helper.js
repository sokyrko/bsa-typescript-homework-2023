class DetailMovie {
    constructor(id, title, description, image, data){
        this._id = id;
        this._title = title;
        this._description = description;
        this._image = image;
        this._data = data;
    }
    getDetail = () => {
        return {
            id: this._id,
            title: this._title,
            description: this._description,
            image: this._image,
            data: this._data,
        }
    }
}
export {DetailMovie};