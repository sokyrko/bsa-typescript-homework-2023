"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.render = void 0;
var helper_1 = require("./helper");
function render() {
    return __awaiter(this, void 0, void 0, function () {
        var api_key, movie_id, storage, currentType, urlSearch, urlTopRated, urlPpopular, urlUpcoming, urlLatest, urlDetail, onRandomMovie, onPopular, onUpcoming, onTopRated, onSearch, onFavoriteMovie, onLoadMore, getCards, getFilmContainer, getFavoriteMovies, http, getBySearch, getDetail, getRandomMovie, getByPopular, getByUpcoming, getByTopRated, handleFavoriteMovies, setCurrentData, addPreloader, onUpdatePage, addContent;
        var _this = this;
        return __generator(this, function (_a) {
            api_key = 'd5e79b4056426ebafbf190f96082c873';
            movie_id = 1;
            storage = window.localStorage;
            currentType = null;
            urlSearch = "https://api.themoviedb.org/3/search/movie?api_key=".concat(api_key);
            urlTopRated = "https://api.themoviedb.org/3/movie/top_rated?api_key=".concat(api_key);
            urlPpopular = "https://api.themoviedb.org/3/movie/popular?api_key=".concat(api_key);
            urlUpcoming = "https://api.themoviedb.org/3/movie/upcoming?api_key=".concat(api_key);
            urlLatest = "https://api.themoviedb.org/3/movie/latest?api_key=".concat(api_key);
            urlDetail = "https://api.themoviedb.org/3/movie/".concat(movie_id, "?api_key=").concat(api_key);
            onRandomMovie = document.querySelector('#random-movie');
            onPopular = document.querySelector('#popular');
            onUpcoming = document.querySelector('#upcoming');
            onTopRated = document.querySelector('#top_rated');
            onSearch = document.querySelector('form #submit');
            onFavoriteMovie = document.querySelector('.navbar-toggler');
            onLoadMore = document.querySelector('#load-more');
            getCards = document.querySelectorAll('.card');
            getFilmContainer = document.querySelector('#film-container');
            getFavoriteMovies = document.querySelector('#favorite-movies');
            http = function (urlStr) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, fetch(urlStr)
                                .then(function (response) { return response.json(); })];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            }); };
            getBySearch = function (urlSearch) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, http(urlSearch)
                            .then(function (data) {
                            var res = data.results;
                            var i = 0;
                            res.map(function (obj) {
                                var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                                var isFav = "#ff000078";
                                if (storage !== null) {
                                    var inStorage_1 = false;
                                    var checkId = Object.keys(storage).forEach(function (key) {
                                        inStorage_1 = storage.getItem(key) === obj.id ? true : false;
                                    });
                                    var isFav_1 = inStorage_1 ? "#FF0019" : "#ff000078";
                                }
                                var addBlock = addContent(info, isFav);
                                var blockWrapper = document.createElement('div');
                                blockWrapper.classList.add("col-lg-3", "col-md-4", "col-12", "p-2");
                                blockWrapper.innerHTML = addBlock;
                                getFilmContainer.append(blockWrapper);
                                i++;
                            });
                        })];
                });
            }); };
            getDetail = function (urlDetail) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, http(urlDetail)
                                .then(function (data) {
                                var res = data;
                                return data;
                            })];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            }); };
            getRandomMovie = (function () {
                // onRandomMovie
                storage.clear();
                if (storage !== null && storage.length) {
                    var lengthStorage = storage.length;
                    var randNum = Math.random() * lengthStorage;
                    var randomKey = storage.key(randNum);
                    var movie_id_1 = storage.getItem(randomKey);
                    var urlString = "https://api.themoviedb.org/3/movie/".concat(movie_id_1, "?api_key=").concat(api_key);
                    var detailInfo = getDetail(urlString);
                    detailInfo.then(function (obj) {
                        var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                        onRandomMovie.querySelector('#random-movie-name').innerHTML = info._title;
                        onRandomMovie.querySelector('#random-movie-description').innerHTML = info._description;
                    });
                }
            })();
            getByPopular = function (urlPopular) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, http(urlPopular)
                                .then(function (data) {
                                var res = data.results;
                                var i = 0;
                                res.map(function (obj) {
                                    var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                                    var isFav = "#ff000078";
                                    if (storage !== null) {
                                        var inStorage_2 = false;
                                        var checkId = Object.keys(storage).forEach(function (key) {
                                            var toNum = +storage.getItem(key);
                                            if (toNum === obj.id)
                                                inStorage_2 = true;
                                        });
                                        isFav = inStorage_2 ? "#FF0019" : "#ff000078";
                                    }
                                    var addBlock = addContent(info, isFav);
                                    var blockWrapper = document.createElement('div');
                                    blockWrapper.classList.add("col-lg-3", "col-md-4", "col-12", "p-2");
                                    blockWrapper.innerHTML = addBlock;
                                    getFilmContainer.append(blockWrapper);
                                    i++;
                                });
                            })];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            }); };
            getByUpcoming = function (urlUpcoming) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, http(urlUpcoming)
                                .then(function (data) {
                                var res = data.results;
                                var i = 0;
                                res.map(function (obj) {
                                    var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                                    var isFav = "#ff000078";
                                    if (storage !== null) {
                                        var inStorage_3 = false;
                                        var checkId = Object.keys(storage).forEach(function (key) {
                                            var toNum = +storage.getItem(key);
                                            if (toNum === obj.id)
                                                inStorage_3 = true;
                                        });
                                        isFav = inStorage_3 ? "#FF0019" : "#ff000078";
                                    }
                                    var addBlock = addContent(info, isFav);
                                    var blockWrapper = document.createElement('div');
                                    blockWrapper.classList.add("col-lg-3", "col-md-4", "col-12", "p-2");
                                    blockWrapper.innerHTML = addBlock;
                                    getFilmContainer.append(blockWrapper);
                                    i++;
                                });
                            })];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            }); };
            getByTopRated = function (urlTopRelated) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, http(urlTopRelated)
                                .then(function (data) {
                                var res = data.results;
                                var i = 0;
                                res.map(function (obj) {
                                    var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                                    var isFav = "#ff000078";
                                    if (storage !== null) {
                                        var inStorage_4 = false;
                                        var checkId = Object.keys(storage).forEach(function (key) {
                                            var toNum = +storage.getItem(key);
                                            if (toNum === obj.id)
                                                inStorage_4 = true;
                                        });
                                        isFav = inStorage_4 ? "#FF0019" : "#ff000078";
                                    }
                                    var addBlock = addContent(info, isFav);
                                    var blockWrapper = document.createElement('div');
                                    blockWrapper.classList.add("col-lg-3", "col-md-4", "col-12", "p-2");
                                    blockWrapper.innerHTML = addBlock;
                                    getFilmContainer.append(blockWrapper);
                                    i++;
                                });
                            })];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            }); };
            handleFavoriteMovies = function () {
                getFavoriteMovies.innerHTML = '';
                if (storage !== null) {
                    Object.keys(storage).forEach(function (key) {
                        var movie_id = storage.getItem(key);
                        var urlString = "https://api.themoviedb.org/3/movie/".concat(movie_id, "?api_key=").concat(api_key);
                        var detailInfo = getDetail(urlString);
                        detailInfo.then(function (obj) {
                            var info = new helper_1.DetailMovie(obj.id, obj.title, obj.overview, obj.poster_path, obj.release_date);
                            var isFav = "#ff000078";
                            if (storage !== null) {
                                var inStorage_5 = false;
                                var checkId = Object.keys(storage).forEach(function (key) {
                                    var toNum = +storage.getItem(key);
                                    if (toNum === obj.id)
                                        inStorage_5 = true;
                                });
                                isFav = inStorage_5 ? "#FF0019" : "#ff000078";
                            }
                            var addBlock = addContent(info, isFav);
                            var blockWrapper = document.createElement('div');
                            blockWrapper.classList.add("col-12", "p-2");
                            blockWrapper.innerHTML = addBlock;
                            getFavoriteMovies.append(blockWrapper);
                        });
                    });
                }
            };
            onSearch.addEventListener('click', function (byTitle) {
                var searchMovies = document.querySelector('form #search').value;
                var searchQuery = "&query=".concat(searchMovies);
                var urlString = "".concat(urlSearch).concat(searchQuery);
                getBySearch(urlString);
                return true;
            });
            onPopular.addEventListener('click', function () {
                if (currentType !== null && currentType.name !== 'urlPpopular')
                    getFilmContainer.innerHTML = '';
                setCurrentData('urlPopular', urlPpopular, 1);
                getByPopular(urlPpopular);
                return true;
            });
            onUpcoming.addEventListener('click', function () {
                if (currentType !== null && currentType.name !== 'urlUpcoming')
                    getFilmContainer.innerHTML = '';
                setCurrentData('urlUpcoming', urlUpcoming, 1);
                getByUpcoming(urlUpcoming);
                return true;
            });
            onTopRated.addEventListener('click', function () {
                if (currentType !== null && currentType.name !== 'urlTopRated')
                    getFilmContainer.innerHTML = '';
                setCurrentData('urlTopRated', urlTopRated, 1);
                getByTopRated(urlTopRated);
                return true;
            });
            onFavoriteMovie.addEventListener('click', function () {
                handleFavoriteMovies();
                return true;
            });
            onLoadMore.addEventListener('click', function () {
                var currentPage = currentType !== null ? currentType.page : 1;
                var currentPath = currentType !== null ? currentType.url : urlPpopular;
                var currentName = currentType !== null ? currentType.name : 'urlPpopular';
                var searchQuery = "&page=".concat(currentPage);
                var urlString = "".concat(currentPath).concat(searchQuery);
                setCurrentData(currentName, currentPath, currentPage + 1);
                switch (currentName) {
                    case 'urlPopular':
                        setCurrentData('urlPopular', currentPath, currentPage + 1);
                        getByPopular(urlString);
                        return;
                    case 'urlTopRated':
                        setCurrentData('urlTopRated', currentPath, currentPage + 1);
                        getByTopRated(urlString);
                        return;
                    case 'urlUpcoming':
                        setCurrentData('urlUpcoming', currentPath, currentPage + 1);
                        getByUpcoming(urlString);
                        return;
                }
                return true;
            });
            setCurrentData = function (name, url, num) {
                return currentType = { name: name, url: url, page: num };
            };
            getFilmContainer.addEventListener('click', function (e) {
                if (e.target.tagName === 'svg' || e.target.tagName === 'IMG') {
                    var id_1 = e.target.parentNode.id;
                    var inStorage_6 = false;
                    var checkId = Object.keys(storage).forEach(function (key) {
                        if (storage.getItem(key) === id_1)
                            inStorage_6 = true;
                    });
                    if (inStorage_6)
                        storage.removeItem("id_".concat(id_1));
                    else
                        storage.setItem("id_".concat(id_1), id_1);
                    onUpdatePage();
                }
            });
            getFavoriteMovies.addEventListener('click', function (e) {
                if (e.target.tagName === 'svg' || e.target.tagName === 'IMG') {
                    var id_2 = e.target.parentElement.id;
                    var inStorage_7 = false;
                    var checkId = Object.keys(storage).forEach(function (key) {
                        if (storage.getItem(key) === id_2)
                            inStorage_7 = true;
                    });
                    if (inStorage_7)
                        storage.removeItem("id_".concat(id_2));
                    else
                        storage.setItem("id_".concat(id_2), id_2);
                    onUpdatePage();
                }
            });
            addPreloader = function () {
                var blockWrapper = document.createElement('div');
                blockWrapper.classList.add("col-lg-3", "col-md-4", "col-12", "p-2");
                blockWrapper.innerHTML = '<i class="fa fa-spinner" aria-hidden="true"></i>';
                return getFilmContainer.prepend(blockWrapper);
            };
            onUpdatePage = function () {
                getFilmContainer.innerHTML = '';
                var currentPage = currentType !== null ? currentType.page : 1;
                var currentPath = currentType !== null ? currentType.url : urlPpopular;
                var currentName = currentType !== null ? currentType.name : 'urlPpopular';
                var searchQuery = "&page=".concat(currentPage);
                var urlString = "".concat(currentPath).concat(searchQuery);
                switch (currentName) {
                    case 'urlPopular':
                        getByPopular(urlString);
                        return;
                    case 'urlTopRated':
                        getByTopRated(urlString);
                        return;
                    case 'urlUpcoming':
                        getByUpcoming(urlString);
                        return;
                }
                return true;
            };
            addContent = function (obj, isFavorite) {
                var insBlock = "\n                    <div class=\"card shadow-sm\" id=\"".concat(obj._id, "\">\n                        <img\n                            src=\"https://image.tmdb.org/t/p/original/").concat(obj._image, "\"\n                        />\n                        <svg\n                            xmlns=\"http://www.w3.org/2000/svg\"\n                            stroke=\"red\"\n                            fill= ").concat(isFavorite, "\n                            width=\"50\"\n                            height=\"50\"\n                            class=\"bi bi-heart-fill position-absolute p-2\"\n                            viewBox=\"0 -2 18 22\"\n                        >\n                            <path\n                                fill-rule=\"evenodd\"\n                                d=\"M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z\"\n                            />\n                        </svg>\n                        <div class=\"card-body\">\n                            <p class=\"card-text truncate\">\n                                 ").concat(obj._description, "\n                            </p>\n                            <div\n                                class=\"\n                                    d-flex\n                                    justify-content-between\n                                    align-items-center\n                                \"\n                            >\n                                <small class=\"text-muted\">").concat(obj._data, "</small>\n                            </div>\n                        </div>\n                    </div>");
                return insBlock;
            };
            return [2 /*return*/];
        });
    });
}
exports.render = render;
